import sys
from skimage import io
import matplotlib.pyplot as plt

image = io.imread(fname=sys.argv[1])

ax = plt.hist(image.ravel(), bins = 256)
plt.show()